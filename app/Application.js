/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Latihan.Application', {
    extend: 'Ext.app.Application',

    name: 'Latihan',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        var loggedIn;
        loggedIn = localStorage.getItem("loggedIn");
        if (!loggedIn) {
                this.overlay = Ext.Viewport.add({
                xtype: 'login',
                floated: true,
                hideOnMaskTap: true,
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },
                centered: true,
                width: '100%',
                height: '100%',
                    // width: Ext.filterPlatform('ie10') ? '100%' : (Ext.os.deviceType == 'Phone') ? 260 : 400,
                    // maxHeight: Ext.filterPlatform('ie10') ? '30%' : (Ext.os.deviceType == 'Phone') ? 220 : 400,
                    // styleHtmlContent: true,
                    // html: '<p>This is a modal, centered and floated panel. hideOnMaskTap is true by default so ' +
                    // 'we can tap anywhere outside the overlay to hide it.</p>',
                    // header: {
                    //     title: 'Overlay Title'
                    // },
                scrollable: true
            });                    
        this.overlay.show(); 
        }   

        document.getElementById('recordCordova').addEventListener('click', globalUtils.startRecordCordova, false);
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
