/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Latihan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Latihan.view.main.MainController',
        'Latihan.view.main.MainModel',
        'Latihan.view.main.List',
        'Latihan.view.form.Users',
        'Latihan.view.group.Carousel',
        'Latihan.view.setting.BasicDataView',
        'Latihan.view.form.Login',
        'Latihan.view.chart.Column',
        // 'Latihan.view.tree.TreeList'
        'Latihan.view.tree.TreePanel'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-archive',
            layout: 'fit',
            items: [{
                xtype: 'chart-corousel'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-archive',
            layout: 'fit',
            items: [{
                // xtype: 'tree-list'
                xtype: 'tree-panel'
            }]
        }]
});
