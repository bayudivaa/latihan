/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Latihan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        var nama = record.data.name;
        var npm = record.data.npm;
        localStorage.setItem('nama', nama);
        localStorage.setItem('npm', npm);
        Ext.Msg.confirm('Konfirmasi', 'Apa kamu yakin '+nama+' ?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama');
        var npm = localStorage.getItem('npm');
        if (choice === 'yes') {
            alert('Terima Kasih sudah memilih YES '+nama+' ('+npm+') ');
        }
        else{
            alert('Jangan memilih NO! '+nama+' ('+npm+') ' );
        }
    }
});
