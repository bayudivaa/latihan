/**
 * This view is an example list of people.
 */
Ext.define('Latihan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Latihan.store.Personnel'
    ],

    title: 'Data Anggota SAYAA',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'NPM', dataIndex: 'npm', width: 120 },
        { text: 'Nama',  dataIndex: 'name', width: 150 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'No HP', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
