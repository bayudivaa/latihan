Ext.define('Latihan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',

    requires: [
        'Ext.plugin.Responsive',
        'Ext.dataview.plugin.ItemTip',        
        'Latihan.store.Personnel'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },
    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [
    {
        docked: 'top',
        xtype: 'toolbar',
        items: [
        {
            xtype: 'searchfield',
            placeHolder: 'Search by NAME',
            name: 'searchfield',
            listeners: {
                change: function ( me, newValue, oldValue, eOpts ) {
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('name', newValue);
                }
            }
        },{
            xtype: 'spacer'
        },{
            xtype: 'searchfield',
            placeHolder: 'Search by NPM',
            name: 'searchfieldnpm',
            listeners: {
                change: function ( me, newValue, oldValue, eOpts ) {
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('npm', newValue);
                }
            }
        },{
            xtype: 'spacer'
        },{
            xtype: 'searchfield',
            placeHolder: 'Search by PHONE',
            name: 'searchfieldphone',
            listeners: {
                change: function ( me, newValue, oldValue, eOpts ) {
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('phone', newValue);
                }
            }
        }]
    },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="img" style="float: left;"> <img src="{photo}" width="100px" height="100px"></div><br><br><div style="margin-left: 120px;"><font size=3 color="blue"><b><u>{npm}</u></b></font><br><font size=5 color="red"><b>{name}</b></font><br><i>{email}<i><br><b>{phone}</b></div><hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Name: </td><td>{name}</td></tr>' +
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>Email: </td><td>{email}</td></tr>' +
                    '<tr><td>Phone: </td><td>{phone}</td></tr>'
                    //'<tr><td vAlign="top">Phone:</td><td><div style="max-height:100px;overflow:auto;padding:1px">{phone}</div></td></tr>'
        }
    }]
});