Ext.define('Latihan.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',    

    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],
    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    listeners: {
        itemtap: function(me , index, target, record, e, e0pts ){
            detailtree = Ext.getCmp('detailtree');
            personnelStore = Ext.getStore('personnel');
            var rec = record.data.text;
            // alert("Anda memilih "+record.data.text);
            if (rec == "Laki-laki"){
                var pilih = "Laki-laki"
                personnelStore.filter('gender', pilih); 
            }
            else if (rec == "Perempuan"){
                var pilih = "Perempuan"
                personnelStore.filter('gender', pilih); 
            }
            else{
                
            }               
        }
    }
});