Ext.define('Latihan.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Kategori',
                    iconCls: 'x-fa fa-home',
                    children: [{
                        text: 'Laki-laki',
                        iconCls: 'x-fa fa-male',
                        leaf: true
                    },{
                        text: 'Perempuan',
                        iconCls: 'x-fa fa-female',
                        leaf: true
                    }]
                }]
            }
        }
    }
});