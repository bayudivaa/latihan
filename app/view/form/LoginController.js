Ext.define('Latihan.view.form.LoginController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.login',

    onLogin : function(){
    	var form = this.getView();
    	var me = this;
    	var username = form.getFields('username').getValue();
    	var password = form.getFields('password').getValue();
    	if (username && password){						// cek jika username dan password ada Valuenya
    		if(username=="bayu" && password=="bayu"){ 	// cek jika username dan password = bayu 
    			localStorage.setItem('username', username);
    			localStorage.setItem('password', password);
    			localStorage.setItem('loggedIn', true);
    			form.hide();
    			Ext.Msg.alert('<center>Login Berhasil</center>','<center>Terima Kasih</center>');
    		}
    		else{
    			Ext.Msg.alert('<center>Login Gagal</center>','<center>Username = bayu, Password = bayu</center>');
    		}
    	}    	
    }

});