Ext.define('Latihan.view.group.Slider', {
    extend: 'Ext.form.Panel',
    xtype: 'slide',

    requires: [
        'Ext.field.Slider',
        'Ext.field.Toggle'
    ],

    scrollable: true,
    shadow: true,
    cls: 'demo-solid-background',
    items: [
        {
            xtype: 'fieldset',
            defaults: {
                labelWidth: '35%',
                labelAlign: 'top'
            },
            items: [
                {
                    xtype: 'sliderfield',
                    name: 'thumb',
                    value: 20,
                    label: 'Single Thumb'
                },
                {
                    xtype: 'sliderfield',
                    name: 'thumb',
                    value: 30,
                    disabled: true,
                    label: 'Disabled Single Thumb'
                },
                {
                    xtype: 'sliderfield',
                    name: 'multithumb',
                    label: 'Multiple Thumbs',
                    values: [10, 70]
                },
                {
                    xtype: 'togglefield',
                    name: 'toggle',
                    label: 'Toggle'
                }
            ]
        }
    ]
});