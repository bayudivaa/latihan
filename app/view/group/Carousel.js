Ext.define('Latihan.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',

    requires: [
        'Ext.carousel.Carousel',

        'Latihan.view.group.Label',
        'Latihan.view.group.Slider',
        'Latihan.view.group.Color'
    ],

    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        items: [{
            xtype: 'placeholderlabel'
        },
        {
            xtype: 'slide'
        },
        {
            xtype: 'thiscolor'
        }]
    }, {
        xtype: 'carousel',
        direction: 'vertical',
        items: [{
            xtype: 'thiscolor'
        },
        {
            xtype: 'placeholderlabel'
        },
        {
            xtype: 'slide'
        }]
    }]
});