Ext.define('Latihan.view.group.CarouselChart', {
    extend: 'Ext.Container',
    xtype: 'chart-corousel',

    requires: [
        'Ext.carousel.Carousel',

        'Latihan.view.chart.Column',
        'Latihan.view.chart.Bar',
        'Latihan.view.chart.Radar'
    ],

    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        items: [{
            xtype: 'column-chart'
        },
        {
            xtype: 'bar-chart'
        },
        {
            xtype: 'radar-chart'
        }]
    }]
});