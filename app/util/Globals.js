Ext.define("Latihan.util.Globals",{
    singleton : true,
    alternateClassName: 'globalUtils',
    version:'1.0',
    // config: {
    //     phppath: 'https://tensaitech.id/pertemuan/resources'
    // },

    // constructor : function(config) {
    //     this.initConfig(config);
    // },    


    startRecordCordova: function(){
        // console.log(navigator);
        let opts = {limit:1};
        navigator.device.capture.captureAudio(globalUtils.captureSuccess, globalUtils.captureError, opts);
    },

    captureSuccess: function(mediaFiles){
        var i, path, len;
        name = mediaFiles[0].name;
        fileURL = mediaFiles[0].fullPath;
        type = mediaFiles[0].type;
        size = mediaFiles[0].size;
        if(size>80000){
            document.getElementById('recordingInfo').textContent = "Rekaman Anda terlalu panjang, silakan coba lagi.";        
        }
        else{
            document.getElementById('recordingInfo').textContent = "Menyimpan rekaman suara...";
        }
    },
    captureError:function(error){
        document.getElementById('msg').textContent = 'Error code: ' + error.code;
    }
});
