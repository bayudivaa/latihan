Ext.define('Latihan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'photo', 'npm', 'name', 'email', 'phone', 'gender'
    ],

    data: { items: [
        { photo: 'resources/3.png', npm: '183510522', name: 'Bayu Diva A', email: "bayudivaa@student.uir.ac.id", phone: "080-000-909", gender: "Laki-laki" },
        { photo: 'resources/2.png', npm: '173510322', name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222", gender: "laki-laki" },
        { photo: 'resources/1.png', npm: '163510542', name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333", gender: "Perempuan" },
        { photo: 'resources/6.png', npm: '183511522', name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444", gender: "laki-laki" },
        { photo: 'resources/5.png', npm: '163510542', name: 'Melani',   email: "melani@yahoo.com",              phone: "874-333-4555", gender: "Perempuan" },
        { photo: 'resources/4.png', npm: '163510542', name: 'Annisa',   email: "annisa@gmail.com",              phone: "666-909-0000", gender: "Perempuan" },
        { photo: 'resources/7.png', npm: '163510542', name: 'Putra',   email: "putra@enterprise.com",           phone: "222-333-4444", gender: "Laki-laki" }
    ]},

    proxy: {
        type: 'memory', 
        //type: 'jsonp'
        // api : {
        //     read : Latihan.utils.Global.getPhppath()+'\namaFileRead.php'
        // }
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
